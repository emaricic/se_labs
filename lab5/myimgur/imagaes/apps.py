from django.apps import AppConfig


class ImagaesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'imagaes'
