from django.http.response import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.shortcuts import render
from django.http import HttpResponse
from .models import Imagaes, Comment


# Create your views here.
def index(request):
    imagaes = Imagaes.objects.order_by('-pub_date')
    context = {
        'all_imagaes': imagaes,
    }
    return render(request, 'imagaes/index.html', context)
def detail(request, image_id):
    image = get_object_or_404(Imagaes, pk=image_id)
    context = {
        'image' : image
    }
    return render(request, 'imagaes/detail.html', context)

def about(request):
    context = {}
    return render(request, 'imagaes/about.html', context)