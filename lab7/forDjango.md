startup:

mkdir venv
cd venv
python -m venv django
source django/Scripts/activate
cd ..
cd myimgur
i tu gdje je requirements.txt u tom folderu pisemo: pip install -r requirements.txt
i onda python manage.py runserver da ga pokrenemo
python manage.py makemigrations
python manage.py migrate



python manage.py startapp accounts - napraviti novi folder tj novu aplikaciju

forme da budu ljepse koristimo pip install django-widget-tweaks
pip freeze - prikaz dodatnih paketa
pip freeze > requirements.txt unesi paket u requirements.txt

MIGRACIJE
moramo napravit migracije kada mijenjamo models.py
python manage.py makemigrations
python manage.py migrate

FIX ZA MIGRACIJE AKO NE ZELI
To solve it I did this (on Ubuntu, you'll need to adapt the commands for Windows):

1. Remove all the migration files

find . -path "*/migrations/*.py" -not -name "__init__.py" -delete

find . -path "*/migrations/*.pyc"  -delete

rm db.sqlite3
python manage.py makemigrations
python manage.py migrate
python manage.py migrate --run-syncdb

winpty python manage.py createsuperuser