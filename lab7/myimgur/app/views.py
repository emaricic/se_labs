from calendar import Calendar
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse
from .models import Image, Comment, Vote, Like
from .forms import ImageForm, CommentForm
# Create your views here.

def index(request):
    images = Image.objects.order_by('-pub_date')
    votes = [ image.vote_by() for image in images ]
    context = { 'images_with_votes': zip(images, votes)}
    return render(request, 'app/index.html', context)


def detail(request, image_id):
    image = get_object_or_404(Image,pk=image_id)
    context = {'image': image, 
                'vote': image.vote_by(),    
               'comments': image.comment_set.all(),
               'form': CommentForm(),
              }
    return render(request, 'app/detail.html', context)

def create_image(request):
    if request.method == 'POST' and request.user.is_authenticated:
        form = ImageForm(request.POST)
        if form.is_valid():
            saved_image = form.save(commit=False)
            saved_image.user = request.user
            saved_image.save()   
            return HttpResponseRedirect(reverse('app:detail', args=(saved_image.id,)))
    else:
        form = ImageForm()
    context = { 'form' : form, 'action':'create'  }
    return render(request, 'app/create_image.html', context)

def edit_image(request, image_id):
    image = get_object_or_404(Image, pk=image_id)
    if image.user == request.user or request.user.is_superuser:
        if request.method == 'POST':
            form = ImageForm(request.POST, instance=image)
            if form.is_valid():
                saved_image = form.save()
                return HttpResponseRedirect(reverse('app:detail', args=(saved_image.id,)))
        else:
            form = ImageForm(instance=image)
        context = { 'form' : form, 'action': 'edit' }
        return render(request, 'app/create_image.html', context)
    else:
        return HttpResponseRedirect(reverse('app:detail', args=(image.id,)))
        
def delete_image(request,image_id):
    image = get_object_or_404(Image, pk=image_id)
    if image.user == request.user or request.user.is_superuser:
        if request.method == 'POST' and request.user.is_authenticated:
            image.delete()
        return HttpResponseRedirect(reverse('app:index'))
    else:
        return HttpResponseRedirect(reverse('app:index'))

def comment(request, image_id):
    if request.method == 'POST' and request.user.is_authenticated:
        image = get_object_or_404(Image, pk=image_id)
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.image = image
            comment.user = request.user
            comment.save()
            return HttpResponseRedirect(reverse('app:detail', args=(comment.image.id,)))        
        else:
            return render(request, 'app/detail.html', {
                'image' : image,
                'comments' : image.comment_set.all(),
                'form' : form,
            })
    else:
        return HttpResponseRedirect(reverse('app:detail',args=(image_id,)))

def like_comment(request, comment_id):
    if request.method == 'POST' and request.user.is_authenticated:
            comment = get_object_or_404(Comment, pk=comment_id)
            like = Like.objects.filter(user=request.user, comment=comment).first()
            if like:
                if like.like == True:
                    like.delete()
                    return HttpResponseRedirect(reverse('app:detail',args=(comment.image.id,)))
                else:
                    like.like == like
            else:
                like = Like(user=request.user, comment=comment, like=True)
            try: 
                like.full_clean()
                like.save()
            except:
                return None
            else:
                return HttpResponseRedirect(reverse('app:detail',args=(comment.image.id,)))
    else:
        return HttpResponseRedirect(reverse('app:index'))

def approve_comment(request, comment_id):
    if request.method == 'POST' and request.user.is_superuser:
        try:
            comment= get_object_or_404(Comment, pk=comment_id)
            comment.approved = True
            comment.save()
        except:
            return None
        else:
            return HttpResponseRedirect(reverse('app:detail', args=(comment.image.id,)))
        
def delete_comment(request,comment_id):
    comment = get_object_or_404(Comment, pk=comment_id)
    if comment.user == request.user or request.user.is_superuser:
        if request.method == 'POST' and request.user.is_authenticated:
            comment.delete()
        return HttpResponseRedirect(reverse('app:detail',args=(comment.image.id,)))
    else:
        return HttpResponseRedirect(reverse('app:detail',args=(comment.image.id,)))
   
def edit_comment(request,comment_id):
    comment = get_object_or_404(Comment, pk=comment_id)
    if comment.user == request.user or request.user.is_superuser:
        if request.method == 'POST':
            form = CommentForm(request.POST, instance=comment)
            if form.is_valid():
                saved_comment = form.save()
                return HttpResponseRedirect(reverse('app:detail', args=(saved_comment.image.id,)))
        else:
            form = CommentForm(instance=comment)
        context = { 'form' : form }
        return render(request, 'app/edit_comment.html', context)
    else:
        return HttpResponseRedirect(reverse('app:detail',args=(comment.image.id,)))

def vote(request, image_id, upvote):
    if request.user.is_authenticated:
        image = get_object_or_404(Image, pk=image_id)
        vote = Vote.objects.filter(user=request.user, image=image).first()
        if vote:
            if vote.upvote == upvote:
                vote.delete()
                return None
            else:
                vote.upvote = upvote
        else:
            vote = Vote(user=request.user, image=image, upvote=upvote)
        try: 
            vote.full_clean()
            vote.save()
        except:
            return None
        else:
            return vote
    else:
        return HttpResponseRedirect(reverse('app:detail',args=(image_id,)))

def upvote(request, image_id):
    if request.method == 'POST' and request.user.is_authenticated:
        vote(request, image_id, True)
    return HttpResponseRedirect(reverse('app:detail', args=(image_id,)))

def downvote(request, image_id):
    if request.method == 'POST' and request.user.is_authenticated:
        vote(request, image_id, False)
    return HttpResponseRedirect(reverse('app:detail', args=(image_id,)))

