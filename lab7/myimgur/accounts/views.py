from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic
from app.models import *

# Create your views here.
class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'

def profile(request, user_id):
    images = Image.objects.filter(user=user_id).all()
    votes = Vote.objects.filter(user_id=user_id).all()
    comments = Comment.objects.filter(user_id=user_id).all()

    context = {
        'images': images,
        'comments': comments,
        'votes': votes,
    }

    return render(request, 'accounts/profile.html', context=context)
